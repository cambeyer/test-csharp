﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.GPU;
using Emgu.CV.Structure;

namespace BlankEmgu
{
    public partial class frmEmgu : Form
    {
        Capture capture = null;
        Boolean resized = false;

        int MIN_VALUE = 150;
        int MAX_WIDTH = 2000;
        int MAX_HEIGHT = 1200;

        public frmEmgu()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;

            capture = new Capture();
            capture.ImageGrabbed += Display_Captured;

            capture.Start();

            MAX_HEIGHT = Screen.FromControl(this).Bounds.Height - 200;
            MAX_WIDTH = Screen.FromControl(this).Bounds.Width - 200;
        }

        void Display_Captured(object sender, EventArgs e)
        {
            try
            {
                Image<Bgr, Byte> frame = capture.RetrieveBgrFrame();
                if (frame != null)
                {
                    if ((txtWidth.Text != "" && txtHeight.Text != "") && (txtWidth.Text.All(char.IsDigit) && txtHeight.Text.All(char.IsDigit)) && (Convert.ToInt32(txtWidth.Text) > MIN_VALUE && Convert.ToInt32(txtHeight.Text) > MIN_VALUE) && (Convert.ToInt32(txtWidth.Text) < MAX_WIDTH && Convert.ToInt32(txtHeight.Text) < MAX_HEIGHT) && (frame.Width != Convert.ToInt32(txtWidth.Text) || frame.Height != Convert.ToInt32(txtHeight.Text)))
                    {
                        frame = frame.Resize(Convert.ToInt32(txtWidth.Text), Convert.ToInt32(txtHeight.Text), Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    }

                    imageBox1.Image = frame;

                    if (!resized)
                    {
                        this.Invoke(new Action(() => this.Size = new Size(this.Width + (frame.Width - imageBox1.Width), this.Height + (frame.Height - imageBox1.Height))));
                        this.Invoke(new Action(() => imageBox1.Width = frame.Width));
                        this.Invoke(new Action(() => imageBox1.Height = frame.Height));
                        this.Invoke(new Action(() => txtWidth.Text = frame.Width + ""));
                        this.Invoke(new Action(() => txtHeight.Text = frame.Height + ""));
                        resized = true;
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            capture.Stop();
            base.OnClosing(e);
        }

        private void txtWidth_TextChanged(object sender, EventArgs e)
        {
            if (txtWidth.Text != "" && txtWidth.Text.All(char.IsDigit) && Convert.ToInt32(txtWidth.Text) > MIN_VALUE && Convert.ToInt32(txtWidth.Text) < MAX_WIDTH)
            {
                imageBox1.Visible = true;
                resized = false;
            }
            else
            {
                imageBox1.Visible = false;
            }
        }

        private void txtHeight_TextChanged(object sender, EventArgs e)
        {
            if (txtHeight.Text != "" && txtHeight.Text.All(char.IsDigit) && Convert.ToInt32(txtHeight.Text) > MIN_VALUE && Convert.ToInt32(txtHeight.Text) < MAX_HEIGHT)
            {
                imageBox1.Visible = true;
                resized = false;
            }
            else
            {
                imageBox1.Visible = false;
            }
        }
    }
}
