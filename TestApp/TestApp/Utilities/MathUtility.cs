﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Utilities
{
    /// <summary>
    /// Class with static functions useful in evalution some math operations
    /// </summary>
    class MathUtility
    {
        /// <summary>
        /// Evalutates n^m assuming that n >= 0 & m >= 0
        /// </summary>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns>n^m</returns>
        public static int IntegerPower(int n, int m)
        {
            //n is out of range
            if (n < 0 || m < 0)
                return 0;

            //exception case: if m = 0 n^0 is 1
            if (m == 0)
                return 1;

            //normal case:
            int value = n;
            for (int i = 1; i < m; i++)
                value *= n;

            return value;
        }
    }
}
