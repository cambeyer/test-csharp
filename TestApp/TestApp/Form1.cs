﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestApp.Utilities;

/*
 * Creator: John Marnon
 * Date Created: 1/12/15
 * Editors: None
 * Last Edited By: Cameron Beyer
 * Last Edited On: 1/14/15
 * 
 * Purpose
 * -------
 * Contains a Windows Form used for demonstration purposes with GitHub and Coding Standards
 */

namespace TestApp
{
    /// <summary>
    /// Windows form setup to display a button that changes the text in a textbox when pressed.
    /// It also demonstrates some coding standards and advise for code structure.
    /// </summary>
    public partial class Form1 : Form
    {
        //Variables should all be declared at the top of the class before the constructor
        ////These variables should be grouped by relevance

        //Sensor Names
        private string sensorLIDAR = "LIDAR";
        private string sensorSensorWithAVeryVeryVeryVeryVeryRidiculouslyLongName = "AnnoyingName(Don't Do This)";

        //Vision Processing Objects
        private int Capture;
        private bool lastCaptureValid = true;

        //To add a summary block before a function easily just go to the line above and press the 
        //forward-slash key '/' three times

        /// <summary>
        /// Base Windows Form Constructor
        /// Use this instead of Form.Load functions as we have had issues with it in the past
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The User presses the form button.  
        /// This should change the form's text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "Hello World")
            {
                textBox1.Text = "Hello World";
            }
            else
            {
                textBox1.Text = "Hello to me";
            }

            //Coding Standards

            //A line of code should not exceed 100 characters (including whitespace) when possible
            ////Ie. try to cut it off by the X at the end of this line                            X

            //Trying to find the largest sensor reading and preform n^n, n*n, n+n, n-n, and n/n
            //---------------------------------------------------------------------------------

            //Initialize array for storing a series of integer values from sensor X
            ////Notice how the funtion provides the info from the summary block
            //////Also Notice that the sensor name variable was placed on a separate line
            //////to keep the code from going off the screen
            int[] sensorData = getDataFromAddressedSensor(
                this.sensorSensorWithAVeryVeryVeryVeryVeryRidiculouslyLongName);

            //Find the maximum value in the array (don't need the index)
            ////Note that operations like this may be common and it may be a good idea to create a
            ////utility class with a function for this that others can use (ie OurMathHelper.findMaxInArray(array) )
            int maxVal = int.MinValue;
            for (int i = 0; i < sensorData.Length; i++)
                maxVal = sensorData[0];
            for (int i = 1; i < sensorData.Length; i++)
            {
                if (sensorData[i] > maxVal)
                {
                    maxVal = sensorData[i];
                }
            }

            //Perform n^n operation with maxVal using a loop method, this does not work with nonIntegers
            //or negative maxVal
            ////Again this may be best used in a utility class
            #region deprecated code
            //int nPowern = 0;
            ////maxVal must be positive to evaluate with this method
            //if (maxVal > 0)
            //{
            //    nPowern = maxVal;
            //    for (int i = 1; i < maxVal; i++)
            //        nPowern *= maxVal;
            //}
            #endregion
            int nPowern = MathUtility.IntegerPower(maxVal, maxVal);

            //Perform basic math operations on maxVal using n*n, n-n, n+n, and n/n
            int nTimesn = maxVal * maxVal;
            int nMinusn = maxVal - maxVal;
            int nPlusn = maxVal + maxVal;
            int nDividen = maxVal / maxVal;
        }
        
        /// <summary>
        /// Gets data from the addressed sensor on the robot.
        /// On average takes 100ms.
        /// </summary>
        /// <param name="sensorAddress">The linguistic identifier for the sensor</param>
        /// <returns>Raw values from the sensor (needs to be converted to user Units)</returns>
        private int[] getDataFromAddressedSensor(string sensorAddress)
        {
            //Calls the robot and returns the information
            return new int[] { 0, 3, 123, -6, 8, 8, -100 };
        }
    }
}
